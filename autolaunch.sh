#!/bin/bash

# configurable
tz="America/New_York"

if [[ $# -gt 0 ]]; then tz=$1; fi

source ~/.profile

# internal config
AUTOLAUNCH_EXT=$AUTOLAUNCH
AUTOLAUNCH_INT=$(pwd)
PID_INDEX=0
TIMESTAMP_INDEX=1

# example directive: "12-20-2020,1-15-2021;7:30,22:30;1,2,3,4,5;7:30,60"

function now {
	echo "$(TZ=$tz date +%s)"
}

function split_on_ifs {
	local split_string=$1
	local ifs=$2
	
	# https://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash#tab-top
	IFS=$ifs read -ra sections <<< "${split_string}"
	
	echo "${sections[@]}"
}

function split { 
	echo $( split_on_ifs $1 "," )
}

function join { 
	local IFS=","
	echo "$*"
}

function split_on_ifs_get_index {
	
	local split_string=$1
	local ifs=$2
	local index=$3
	
	local sections=( $( split_on_ifs $split_string $ifs ) )
	
	echo "${sections[$index]}"
}

function calendar_boundaries {
	echo $( split_on_ifs_get_index $directive ";" 0 )
}

function intraday_boundaries {
	echo $( split_on_ifs_get_index $directive ";" 1 )
}

function days {
	echo $( split_on_ifs_get_index $directive ";" 2 )
}

function launch_times {
	echo $( split_on_ifs_get_index $directive ";" 3 )
}

function date_to_seconds {
	echo "$( TZ=$tz date -d $1 +%s )"
}

#function seconds_from_midnight {
#	
#	# https://unix.stackexchange.com/questions/146550/how-do-i-find-seconds-since-midnight
#	eval "$(TZ=$tz date +'today=%F now=%s')"
#	local midnight=$(TZ=$tz date -d "$today 0" +%s)
#	echo "$((now - midnight))"
#	
#}

function seconds_from_hh_mm {
	local hh_mm=$1
	
	# https://stackoverflow.com/questions/9178032/convert-hhmmss-mm-to-seconds-in-bash
	# date +'%s' -d "01:43:38.123"
	echo $(TZ=$tz date +'%s' -d $hh_mm)
	
}

function seconds_from_timed_schedule {
	local hh_mm_times=( $( split $1 ) )
	
	for ((i=0; i<${#hh_mm_times[@]}; i++)); do
		local secs[i]=$( seconds_from_hh_mm ${hh_mm_times[i]} )
	done
	
	echo "${secs[@]}"
}

function seconds_from_intervalic_schedule {
	# https://stackoverflow.com/questions/36439056/how-to-add-elements-to-an-array-in-bash-on-each-iteration-of-a-loop
	
	local first_launch_hh_mm=$1
	local interval=$2
	
	local interval_seconds=$(($interval * 60))
	
	# while additional launch time <= seconds_from_midnight <- 23:59:59
	local next_launch=$( seconds_from_hh_mm $first_launch_hh_mm )
	local end_of_day=$( seconds_from_hh_mm "23:59:59" )
	
	local i=0
	while [[ $next_launch -lt $end_of_day ]]; do
		
		# add next_launch to launch times array
		launches[i]=$next_launch
		
		# increment value of next_launch
		next_launch=$(($next_launch + $interval_seconds))
		((i++))
		
	done
	
	echo "${launches[@]}"
}

function within_bounds_inclusive {
	# for calendar filtering
	
	local now=$1
	local start=$2
	local end=$3
	
	# if no start boundary or now greater than start boundary
	if [[ -z $start || $now -ge $start ]]
	then
		# check end boundary
		if [[ -z $end || $end -ge $now ]]
		then
			true; return
		fi
	fi

	false	
}

function within_bounds_exclusive {
	# for intra-day filtering
	local now=$1
	local start=$2
	local end=$3
	
	# if no start boundary or now greater than start boundary
	if [[ -z $start || $now -ge $start ]]
	then
		# check end boundary
		if [[ -z $end || $end -gt $now ]]
		then
			true; return
		fi
	fi
	
	false	
}

function within_calendar_bounds {
	# passed arguments
	local directive=$1
	local now=$2
	
	# calculated
	local date_bounds=$( calendar_boundaries  $directive )
	local start_date_string=$( split_on_ifs_get_index $date_bounds "," 0 )
	local end_date_string=$( split_on_ifs_get_index $date_bounds "," 1 )
	
	local start_date_seconds=$( date_to_seconds  $start_date_string )
	local end_date_seconds=$( date_to_seconds  $end_date_string )
	
	# resolves to implicit return value (true/false)
	within_bounds_inclusive $now $start_date_seconds $end_date_seconds
}

function within_intraday_bounds {
	# passed arguments
	local directive=$1
	local now=$2
	
	# calculated
	local hour_bounds=$( intraday_boundaries  $directive )
	local start_hour_string=$( split_on_ifs_get_index $hour_bounds "," 0 )
	local end_hour_string=$( split_on_ifs_get_index $hour_bounds "," 1 )
	
	local start_time_seconds=$( seconds_from_hh_mm $start_hour_string )
	local end_time_seconds=$( seconds_from_hh_mm $end_hour_string )
	
	# implicit return value
	within_bounds_exclusive $now $start_time_seconds $end_time_seconds
}

function fname {
	# https://unix.stackexchange.com/questions/211834/slash-and-backslash-in-sed
	echo "$1" | sed 's= =\-=g; s=/=\-=g'
}

function most_recent_scheduled_launch_from_seconds_list {
	
	local launch_seconds_list=( $( split  $1 ) )
	local now=$2
	
	local len=${#launch_times[@]}
	
	for ((i=0; i<len; i++)); do
		if [[ ${launch_seconds_list[i]} -lt $now ]]; then t=${launch_seconds_list[i]}; fi
	done
	
	# empty if no suitable launch times found -- check -z in calling function
	echo "${t}"
}

function record_path {
	local vector=$1
	local fn=$( fname $vector )
	local record="${AUTOLAUNCH_INT}/launch_records/${fn}"
	
	echo "${record}"
}

function get_last_line_of_record {
	local record=$1
	local last_line_vals=( $( tac $record | egrep -m 1 . ) )
	
	echo "${last_line_vals[@]}"
}

function get_time_of_last_launch {
	local record=$1
	
	local last_line_vals=( $( get_last_line_of_record $record) )
	
	echo "${last_line_vals[$TIMESTAMP_INDEX]}"
}

function get_pid_of_last_launch {
	local record=$1
	
	local last_line_vals=( $( get_last_line_of_record $record) )
	
	echo "${last_line_vals[$PID_INDEX]}"
}

function last_recorded_launch_for_vector {
	local vector=$1
	local record=$( record_path $vector )
	
	if [[ ! -f $record ]]; then 
		return 
		
	else
		# look up time of last launch
		# https://stackoverflow.com/questions/2638912/how-do-i-get-the-last-non-empty-line-of-a-file-using-tail-in-bash
		last_line_vals=( $( get_last_line_of_record $record ) )
		last_launch=${last_line_vals[$TIMESTAMP_INDEX]}
		
		echo "${last_launch}"
	fi
}

function launch_day {
	# passed
	local directive=$1
	
	# calculated
	local dow=$(TZ=$tz date +%u)
	local days_list=$( days $directive )
	IFS="," read -ra days <<< "$days_list"
	
	# https://stackoverflow.com/questions/3685970/check-if-a-bash-array-contains-a-value
	if [[ " ${days[@]} " =~ " ${dow} " ]]; then true; return; fi; false
}

function launch_app {
	local app=$1
	
	# https://serverfault.com/questions/205498/how-to-get-pid-of-just-started-process
	local pid_line=($( sh -c "echo $$; exec sh ${app} &" ))
	local pid=${pid_line[0]}
	
	echo "${pid}"
}

function record_launch {
	local vector=$1
	local pid=$2
	local now_seconds=$( now )
	local record_path=$( record_path $vector )
	
	touch $record_path
	
	# corresponding to PID_INDEX and TIMESTAMP_INDEX
	echo "${pid} ${now_seconds}" >> $record_path
}

function launch_from_file {
	
#	now_seconds=$(TZ=$tz date +%s)
	now_seconds=$( now )
	
	while read line; do

		# get launch vector, directive and path
		vector_directive=($line)
		
		vector=${vector_directive[0]}
		directive=${vector_directive[1]}
		
		# a launch command with no directive is a start-forget process;
		# if it has ever been started before, move on
		if [[ -z $directive && ! -z $( record_path $vector ) ]]; then continue; fi
		
		app_path="${AUTOLAUNCH_EXT}/apps/${vector}"
		record_path=$( record_path $vector )
		
		echo "Vector: ${vector}"
		echo "Directive: ${directive}"
		echo "App path: ${app_path}"
		echo "Record path: ${record_path}"
		echo "fname: $( fname ${vector} )"

		if ! within_calendar_bounds $directive $now_seconds
		then
			echo "Outside of calendar bounds"
			continue
		else
			echo "Within date calendar bounds"
		fi
		
		if ! within_intraday_bounds $directive $now_seconds
		then
			echo "Not within intra-day bounds"
			continue 
		else
			echo "Within intra-day bounds"
		fi
		
		if ! launch_day $directive
		then
			echo "Wrong day of the week"
			continue 
		else
			echo "Valid day of the week"
		fi
		
		# check if time for launch
		launch_schedule=$( launch_times $directive )
		
		echo "Launch schedule: ${launch_schedule}"
		
		if ! [[ -z $launch_schedule ]]
		then
			# at least one time specified
			launch_times=( $( split $launch_schedule ) )
			
			echo "Launch times: ${launch_times[@]}"
			
			len=${#launch_times[@]}
			
			# get list of launch times in seconds
			if [[ len -eq 1 || len -ge 3 || "${launch_times[1]}" =~ ":" ]]
			then
				# list of pure hh:mm-formatted times, e.g., "9:30" or "9:30,14:30" or "9:30,14:30,22:00"
				echo "List of times"
				launch_times_seconds=( $( seconds_from_timed_schedule $( join $launch_times ) ) ) # leads to repeated split...
				
			else
				# intervalic schedule, e.g., "9:30,10"
				echo "Intervalic"
				launch_times_seconds=( $( seconds_from_intervalic_schedule ${launch_times[0]} ${launch_times[1]} ) )
				
			fi
			
			echo "Launch times seconds: ${launch_times_seconds[@]}"
			
			# look up last (passed) launch time, which may be empty
			most_recent_scheduled_launch=$( most_recent_scheduled_launch_from_seconds_list $( join ${launch_times_seconds[@]} ) $now_seconds )
			
			# may be empty if "most recently passed" scheduled launch time is in the future
			if [[ -z $most_recent_scheduled_launch ]]
			then 
				echo "No launch scheduled yet for today"
				continue
			fi
			
			# retrieve historical launch data -- consider moving to support function
			latest_recorded_launch=$( last_recorded_launch_for_vector  $record_path )
			
			if [[ ! -z $latest_recorded_launch && $latest_recorded_launch -gt $most_recent_scheduled_launch ]]
			then 
				echo "Process already launched since last scheduled launch time"
				continue 
			fi
			
			# last check -- if process still/already running, continue
			PID=$( get_pid_of_last_launch $record_path )
			if [[ ! -z $PID && ! -z $( ps -p $PID -o comm= ) ]]
			then 
				echo "Process still running, skipping."
				continue
				
			else
				echo "Process ${PID} not found, relaunching."
			
			fi
			
			# if we reach this point, we know that...
			# 1) there was at least one scheduled launch for the day
			# 2) we're within calendar and intra-day bounds, and we're on the right day of the week
			# 3) we've eclipsed the most recently scheduled launch time (due for launch)
			# 4) no launch was recorded since the launch became due, and
			# 5) the process is not still running (via pid).
			
			# from here we fall through to launch the process
			
		else # TODO fix duplicate code from above (move dup. block outside of if block)
			
			# no schedule specified -- either start-forget that hasn't been launched before or monitored process
			PID=$( get_pid_of_last_launch $record_path )
			if [[ ! -z $PID && ! -z $( ps -p $PID -o comm= ) ]]
			then 
				echo "Process still running, skipping."
				continue
				
			else
				echo "Process ${PID} not found, relaunching."
				# fall through to launch
			fi
		fi

		# if we made it here, all excluding conditions were passed and we're cleared to launch from vector according to directive
		# https://serverfault.com/questions/205498/how-to-get-pid-of-just-started-process
		pid=$( launch_app $app_path )
		record_launch $vector $pid
		
		echo "Launched ${vector} with pid ${pid} and record $( fname $vector )"
		
	done <launch_commands
}





# testing area...

function generate_list_and_return {
	
	for ((i=0; i<=5; i++)); do
		local my_arr[i]=$i
	done
	
	echo "${my_arr[@]}"
}

function pass_through_list {
#	list=( $( split $1 ) )
#	
#	echo "${list[@]}"
	
	echo $( split $1 )
}

#function join { local IFS=","; echo "$*"; }
#function join_by { local d=$1; shift; local f=$1; shift; printf %s "$f" "${@/#/$d}"; }

#in_list="9:30,14:30"
#
#echo "in_array: ${in_list}"
#
#out_array=( $( pass_through_list  ${in_list}) )

#echo "out_array: ${out_array[@]}"

#arr=( 'this' 'that' 'the other' 'these' 'those')
#echo "$( join ${arr[@]} )"




#lts=( $( seconds_from_intervalic_schedule 9:30 10 ) )
#
#echo "len lts: ${#lts[@]}"
#echo "lts: ${lts[@]}"



launch_from_file 