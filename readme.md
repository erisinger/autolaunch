# Autolauncher

Autolauncher is a tool under development at the University of Massachusetts Amherst.  It provides a simple way for users of a pre-defined VM to schedule and launch processes within the VM without needing access to the VM itself.

## Launch directives

A launch directive is a string that tells the autolauncher when and how to run a launchable process.

A launch directive includes 1) calendar boundaries, 2) intra-day boundaries, 3) launch days of the week, 4) a list of launch times or a launch time with interval units between launches.

A launch directive looks like this: "12-20-2020,1-15-2021;7:30,22:30;1,2,3,4,5;7:30,60"

In detail:

### 1) calendar boundaries: 12-20-2020,1-15-2021

These boundaries indicate that the process will be launched as directed between these dates, inclusive.  

Either or both boundaries can be left blank.  A single boundary date will be interpreted as a start date for a process that will launch once or repeatedly ad infinitum.  A two-element list with an empty firt element will be launched today and will stop on the date specified by the second element.  If the calendar boundary is left completely empty, the process will start today and run once or repeat indefinitely.

### 2) intra-day boundaries: 7:30,22:30

These boundaries represent daily start and stop times for intervalic launches.

Either or both boundaries can be left blank.  A single boundary time represents a start time, with an end time of midnight.  A two-element list whose first element is empty represents a process that can launch at midnight and will stop at the time specified by the second element.  If both boundaries are blank, the process can launch any time, day or night.

The intra-day boundaries should be thought of as a mask that can block a process from being launched outside of a specified windows.

In the example above, a process can be run between 7:30 am (inclusive) and 10:30 pm (exclusive).

### 3) launch days of the week: 1,2,3,4,5

Which days of the week a process should launch, where 1 is Monday, 7 is Sunday.

The example will launch a process only on weekdays.  Leave launch days empty for a process that should launch every day, or which only launches once.

### 4) launch times: 7:30,60

A list of daily times to launch a process *or* an initial launch time and an interval in minutes between launches.

A single launch time represents a time that the process will launch; it will not relaunch on the same day.  A multi-element comma-separated list whose elements consist of valid 24-hour times will launch at the specified times during the day.  A two-element list whose first element is a valid 24-hour time and whose second element is an integer will launch at the time specified by the first element and again according to a minute interval specified by the second element, up to the intra-day stop time (if specified).


## Launch vectors

A launch vector is a unique path-and-parameter command that points to a runnable process.  These processes should be dropped into the Autolauncher > apps directory to ensure that they are available as expected within the VM.

A file called launch_vectors can be found in the Autolauncher root directory.  For each process to be launched, add a line with a launch vector followed by a launch directive.  The autolauncher will launch listed processes accordingly.


## Special cases

An launch directive consisting of empty elements (e.g., ';;;') is a *monitored process*, which is defined as one that starts as soon as possible and should run without disruption.  The autolauncher will track its PID, and if the process stops, the launcher will automatically restart it.

A completely empty launch directive is a *start-forget* process.  It will be run once, immediately on the next wakeup of the autolauncher (every minute), and never again. 